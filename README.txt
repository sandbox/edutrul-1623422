
-- SUMMARY --

With Hidden Author module you can post anonymous nodes and by having the right
permissions.
Once you post anonymously other users may not see your ursername because of this
awesome module.
Imagine that you are very popular in a Forum XYZ. And you've just seen a movie 
that you didn't like and you want to create a forum topic cursing about that
movie. 
But you do not want that other users see your posts. But if Hidden Author module
is enabled and the proper permissions are configured correctly, you may check a
button which says "Post anonymously". And you will see that your post is 
anonymous.
So people will not see your username in forum topic.

For a full description visit the sandbox page:
  http://drupal.org/sandbox/edutrul/1623422
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/1623422


-- INSTALLATION --

1. Copy the Hidden Author module to your modules directory and enable it 
   on the Modules page (admin/modules).

2. Set Hidden Author permissions on the following page:
   (admin/people/permissions).


-- CONFIGURATION --

  Administer Hidden Author - Allows you to configure Hidden Author settings by 
  checking which content type you want to use them.
  
  Post anonymous nodes - Allows you to post anonymous nodes which gives you 
  possibility to check or not if a node will be anonymous.
  
  View hidden author - Allows you to view hidden author.


-- CREDITS --

Designed and Developed by Santex Group (http://santexgroup.com @edutrul) and @develCuy.
