<?php
/**
 * @file
 * Allows you to select which content type will be selected as anonymous.
 */

/**
 * Form to enable content types that will use "posting as anonymous".
 */
function hidden_author_list_content_type() {
  $form['hidden_author_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable the "Post anonymous check"'),
    '#description' => t('Enable the "Post anonymous check" in the following content types above.'),
    '#default_value' => variable_get('hidden_author_content_types', array()),
    '#options' => hidden_author_get_content_types(),
  );
  return system_settings_form($form);
}

/**
 * This function is called of hidden_author_list_content_type.
 *
 * @return array
 *   list of content types.
 */
function hidden_author_get_content_types() {
  $ct_list = node_type_get_types();
  foreach ($ct_list as $ct) {
    $op_list[$ct->type] = $ct->name;
  }
  return $op_list;
}
